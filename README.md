This is a FullStack test assignment from PovioLabs.

## Setup mongoDB, nodeJS and reactApp

You can setup mongodb with docker-compose command, so in the root directory, run:
#### `docker-compose up`

To install dependencies for nodejs and react, run:
#### `npm install`

Then, to run nodejs:
#### `node server/app.js`

Lastly, to run reactapp (port 3000):
#### `npm start`

The app should now be accessible at http://localhost:3000/

## Testing

Server testing:
#### `npm run testserver`

Client testing:
#### `npm test`
