import { observable, action, runInAction, decorate } from 'mobx'
import ApiService from '../services/ApiService'

class RegisterStore {
    error = false
    success = false
    errors = {}

    async register(params) {
        const res = await ApiService.apiCall('/signup', 'POST', false, params)
        if(res.status === 201) {
            runInAction(() => {
                this.success = true
                this.error = false
                this.errors = {}
            })
        } else {
            runInAction(() => {
                this.success = false
                this.error = true
                this.errors = res.body
            })
        }
    }
}
decorate(RegisterStore, {
    error: observable,
    success: observable,
    errors: observable,
    register: action
});

export default new RegisterStore()
export { RegisterStore }