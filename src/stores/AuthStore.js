import { observable, action, runInAction, decorate, toJS } from 'mobx'
import ApiService from '../services/ApiService'
import StorageService from '../services/StorageService'
import history from '../services/history'

class AuthStore {
    authenticated = false
    loading = true
    error = false
    success = false
    errors = {}
    currentUser = null
    profileErrors  = null

    async login(params) {
        const res = await ApiService.apiCall('/login', 'POST', false, params)
        if(res.status === 201) {
            StorageService.setToken(res.body.token)
            StorageService.removeCurrentUser()
            runInAction(() => {
                this.success = true
                this.authenticated = true
                this.error = false
                this.loading = false
            })
        } else {
            runInAction(() => {
                this.success = false
                this.authenticated = false
                this.error = true
                this.loading = false
                this.errors = res.body
            })
        }
    }

    async logout() {
        await ApiService.apiCall('/logout', 'POST', false, null)
        StorageService.removeToken()
        runInAction(() => {
            this.authenticated = false
            this.error = false
            this.loading = false
            this.currentUser = null
            history.push('/')
        })
    }

    async changePassword(params) {
        const res = await ApiService.apiCall('/me/update-password', 'POST', StorageService.getToken(), params)
        if(res.status === 201) {
            runInAction(() => {
                this.error = false
                this.loading = false
                this.success = true
            })
        } else {
            runInAction(() => {
                this.error = true
                this.success = false
                this.errors = res.body
            })
        }
    }

    async likeUnlikeUser(method, userId) {
        const res = await ApiService.apiCall('/user/' + userId + '/' + method, 'POST', StorageService.getToken(), null)
        if(res.status === 201) {
            runInAction(() => {
                this.error = false
                this.loading = false
            })
        } else {
            runInAction(() => {
                this.error = true
                this.loading = false
                this.errors = res.body
            })
        }
    }

    async fetchProfile() {
        try {
            if (!StorageService.getToken()) {
                throw new Error('No token provided!')
            }
            if (StorageService.getCurrentUser()) {
                this.currentUser = JSON.parse(StorageService.getCurrentUser())
                this.loading = false
                this.error = false
                return
            }
            this.loading = true
            const res = await ApiService.current_user(StorageService.getToken())
            runInAction(() => {
                this.currentUser = toJS(res).user
                StorageService.setCurrentUser(toJS(res).user)
                this.loading = false
                this.error = false
            })
        } catch (e) {
            runInAction(() => {
                console.error(e.message)
                StorageService.removeToken()
                this.error = false
                this.currentUser = null
                this.loading = false
            })
        }
    }
}
decorate(AuthStore, {
    authenticated: observable,
    loading: observable,
    error: observable,
    success: observable,
    errors: observable,
    currentUser: observable,
    profileErrors: observable,
    fetchProfile: action,
    login: action,
    logout: action,
});

export default new AuthStore()
export { AuthStore }