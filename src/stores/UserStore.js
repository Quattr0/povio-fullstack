import { observable, action, runInAction, decorate, toJS } from 'mobx'
import ApiService from '../services/ApiService'

class UserStore {
    isLoading = true
    isFailure = false

    async getUsers() {
        try {
            const res = await ApiService.apiCall('/most-liked', 'GET', false, null)
            return toJS(res.body).users
        } catch (e) {
            runInAction(() => {
                this.isLoading = false
                this.isFailure = true
            })
        }
    }
}
decorate(UserStore, {
    isLoading: observable,
    isFailure: observable,
    users: observable,
    getUsers: action
});

export default new UserStore()
export { UserStore }