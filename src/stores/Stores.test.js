import {observe, configure} from 'mobx';
import AuthStore from './AuthStore';
import RegisterStore from './RegisterStore';
import UserStore from './UserStore';

describe('Stores tests', () => {

	beforeEach(() => {
		configure({
			enforceActions: 'always'
		});
	});

	it('should fail to login non-existing user', async () => {

		await AuthStore.login({username: 'user', password: 'pass'});

		expect(AuthStore.error).toEqual(true);
		expect(AuthStore.authenticated).toEqual(false);
		expect(AuthStore.success).toEqual(false);
		expect(typeof AuthStore.errors).toBe('object');
		expect(AuthStore.errors.message).toEqual('User not found.');
	});

	it('should register a new user', async () => {

		await RegisterStore.register({username: 'user', password: 'pass'});

		expect(RegisterStore.success).toEqual(true);
		expect(RegisterStore.error).toEqual(false);

	});

	it('should login a newly created user', async () => {

		await AuthStore.login({username: 'user', password: 'pass'});

		expect(AuthStore.error).toEqual(false);
		expect(AuthStore.authenticated).toEqual(true);
		expect(AuthStore.success).toEqual(true);

	});

	it('should get all users', async () => {

		const data = await UserStore.getUsers();

		expect(typeof data).toBe('object');
		expect(data[0].username).toEqual('user');

	});

});