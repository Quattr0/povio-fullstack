const routes = {
    sign_up: '/signup',
    login: '/login',
    current_user: '/me',
    user_details: '/user/:id',
    user_edit: '/user/:id/edit',
    users: '/users'
}

export default routes;