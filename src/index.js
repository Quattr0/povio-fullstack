import React from 'react'
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react'
import { Router, Switch, Route } from 'react-router'
import history from './services/history'
import authStore from './stores/AuthStore'
import userStore from './stores/UserStore'
import registerStore from './stores/RegisterStore'
import routes from './routes'

import App from './App'
import Home from './components/Home'
import Login from './components/Login'
import Register from './components/Register'
import CurrentUser from './components/CurrentUser'
import UserList from './components/UserList';

const stores = {
    userStore,
    authStore,
    registerStore
};

ReactDOM.render(
    <Provider { ...stores }>
        <App>
            <Router history={history}>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path={routes.login} component={Login} />
                    <Route path={routes.sign_up} component={Register} />
                    <Route path={routes.current_user} component={CurrentUser} />
                    <Route path={routes.users} component={UserList} />
                </Switch>
            </Router>
        </App>
    </Provider>,
    document.getElementById('root')
);