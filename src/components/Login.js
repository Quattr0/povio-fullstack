import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { Redirect } from 'react-router'
import { Link } from 'react-router-dom'
import StorageService from '../services/StorageService'
import history from '../services/history'
import routes from '../routes'

class Login extends Component {

    handleLoginSubmit = async (e) => {
        e.preventDefault()
        await this.props.authStore.login({
            username: this.username.value,
            password: this.password.value,
        })
    }

    async componentWillReact() {
        if(this.props.authStore.authenticated) {
            await this.props.authStore.fetchProfile()
            history.push('/')
        }
    }

    render() {
        const authStore = this.props.authStore
        if (StorageService.getToken() || authStore.authenticated) {
            return <Redirect to='/' />
        }
        return (
            <div className='login container level' style={{ flexDirection: 'column', padding: '50px 0' }}>
                <h2 className="title">Login</h2>
                <form className="login-form has-text-centered" onSubmit={this.handleLoginSubmit}>
                    <div className="field has-addons">
                        <div className="control">
                            <input className="input" type="text" required placeholder="Username" ref={input => this.username = input} />
                        </div>
                    </div>
                    <div className="field has-addons">
                        <div className="control">
                            <input className="input" type="password" required placeholder="Password" ref={input => this.password = input} />
                        </div>
                    </div>
                    <button type="submit" className="button is-link" style={{ margin: '10px' }}>Login</button>
                </form>
                {
                    authStore.error &&
                    <div className='login-error has-text-danger has-text-centered'>
                        <p>Login error!</p>
                        <p>{this.props.authStore.errors.message}</p>
                    </div>
                }
                <Link to={routes.sign_up}>Not registered? Sign up!</Link>
            </div>
        )
    }

}

Login = inject('authStore')(observer(Login))
export default Login