import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { Redirect } from 'react-router'
import routes from '../routes'
import StorageService from '../services/StorageService'

class CurrentUser extends Component {

    handleChangePasswordSubmit = async (e) => {
        e.preventDefault()
        await this.props.authStore.changePassword({
            currPassword: this.currPassword.value,
            newPassword: this.newPassword.value,
        })
    }

    render() {

        if (StorageService.getToken()) {
            const authStore = this.props.authStore
            
            let username = '';
            let likes = '';
            if (authStore.currentUser) {
                username = authStore.currentUser.username
                likes = authStore.currentUser.likes
            }            

            return (
                <div className="container" style={{ padding: '30px' }}>
                    <label className="label">Your username:</label>
                    <p className="subtitle">{ username }</p>
                    <label className="label">Your likes:</label>
                    <p className="subtitle">{ likes }</p>
                    <label className="label">Change password:</label>
                    <form className="change-password-form" onSubmit={this.handleChangePasswordSubmit}>
                        <div className="field has-addons">
                            <div className="control">
                                <input className="input" type="password" required placeholder="Current password" ref={input => this.currPassword = input} />
                            </div>
                        </div>
                        <div className="field has-addons">
                            <div className="control">
                                <input className="input" type="password" required placeholder="New password" ref={input => this.newPassword = input} />
                            </div>
                        </div>
                        <button type="submit" className="button">Submit</button>
                        {
                            authStore.error &&
                            <div className='login-error has-text-danger'>
                                <p>{this.props.authStore.errors.message}</p>
                            </div>
                        }
                        {
                            authStore.success &&
                            <div className='register-success has-text-success'>
                                <p>Password change successful!</p>
                            </div>
                        }
                    </form>
                </div>
            )
        }
        return <Redirect to={routes.login} />
    }

}
CurrentUser = inject('authStore')(observer(CurrentUser))
export default CurrentUser