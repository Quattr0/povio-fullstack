import React, { Component } from 'react'
import { Redirect } from 'react-router'
import { Link } from 'react-router-dom'
import { observer, inject } from 'mobx-react'
import routes from '../routes'
import StorageService from '../services/StorageService'

class Register extends Component {

    handleRegisterSubmit = async (e) => {
        e.preventDefault();
        await this.props.registerStore.register({
            username: this.username.value,
            password: this.password.value,
        })
    }

    render() {
        const registerStore = this.props.registerStore
        if (StorageService.getToken()) {
            return <Redirect to='/' />
        }
        return (
            <div className='register container level' style={{ flexDirection: 'column', padding: '50px 0' }}>
                <h2 className="title">Register</h2>
                <form className="register-form has-text-centered" onSubmit={this.handleRegisterSubmit}>
                    <div className="field has-addons">
                        <div className="control">
                            <input className="input" type="username" required placeholder="Username" ref={(input) => this.username = input} />
                        </div>
                    </div>
                    <div className="field has-addons">
                        <div className="control">
                            <input className="input" type="password" required placeholder="Password" ref={(input) => this.password = input} />
                        </div>
                    </div>
                    <button type="submit" className="button is-link" style={{ margin: '10px' }}>Register</button>
                </form>
                {
                    registerStore.error &&
                    <div className='register-error has-text-danger has-text-centered'>
                        <p>Registration error!</p>
                        <p>{this.props.registerStore.errors.message}</p>
                    </div>
                }
                {
                    registerStore.success &&
                    <div className='register-success has-text-success has-text-centered'>
                        <p>Your registration was successful!</p>
                        <p><Link className="button" to={routes.login}>Sign in!</Link></p>
                    </div>
                }
            </div>
        )
    }

}

Register = inject('registerStore')(observer(Register))
export default Register