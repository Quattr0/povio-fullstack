import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import StorageService from '../services/StorageService'

class UserList extends Component {

    // For async rendering
    state = {
        users: null,
    };

    componentDidMount() {
        this._loadAsyncData();
    }

    componentDidUpdate() {
        if (this.state.users === null) {
            this._loadAsyncData();
        }
    }

    componentWillUnmount() {
        if (this._asyncRequest) {
            this._asyncRequest.cancel();
        }
    }

    _loadAsyncData() {
        const userStore = this.props.userStore
        this._asyncRequest = userStore.getUsers().then(
            (users) => {
                this._asyncRequest = null
                this.setState({users})
            }
        )
    }

    handleLike = async (userId, e) => {
        e.preventDefault()
        await this.props.authStore.likeUnlikeUser('like', userId)
        this.setState({users: null})
    }

    handleUnlike = async (userId, e) => {
        e.preventDefault()
        await this.props.authStore.likeUnlikeUser('unlike', userId)
        this.setState({users: null})
    }

    render() {
        if (!this.state.users) {
            return <div>Loading...</div>;
        }
        if (this.state.users.length < 1) {
            return (
                <div className="level" style={{ justifyContent: 'center' }}>
                    <h2 className="subtitle">No users registered in the app.</h2>
                </div>
            )
        }
        const currentUser = this.props.authStore.currentUser
        var html = this.state.users.map((user, i) => {
            return (
                <div className="level" style={{ justifyContent: 'center' }} key={i}>
                    <span className="is-size-5">{ user.username }</span>
                    <span className="is-size-7" style={{ margin: '0 20px' }}>Likes: { user.likes }</span>
                    {
                        StorageService.getToken() && currentUser._id !== user._id && !user.likedBy.includes(currentUser._id) &&
                        <button className="button" onClick={(e) => this.handleLike(user._id, e)}>Like</button>
                    }
                    {
                        StorageService.getToken() && currentUser._id !== user._id && user.likedBy.includes(currentUser._id) &&
                        <button className="button is-link" onClick={(e) => this.handleUnlike(user._id, e)}>Unlike</button>
                    }
                </div>
            )
        })
        return (
            <div className="container" style={{ padding: '30px 0' }}>
                { html }
            </div>
        )
    }
}
UserList = inject('userStore', 'authStore')(observer(UserList))
export default UserList