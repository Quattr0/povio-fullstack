import React from 'react'

const Header = (props) => {
    return (

        <nav className="navbar is-primary" aria-label="main navigation">
            <div className="navbar-menu">
                <div className="navbar-start">
                    <a className="navbar-item" href="/">
                        Home
                    </a>
                    <a className="navbar-item" href="/me">
                        My Profile
                    </a>
                    <a className="navbar-item" href="/users">
                        Most Liked
                    </a>
                </div>
                <div className="navbar-end">
                    <div className="navbar-item">
                        <a className="button is-light" href="" onClick={(e) => { e.preventDefault(); props.logout(); }}>
                            Logout
                        </a>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default Header