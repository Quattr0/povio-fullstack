import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { Link } from 'react-router-dom'
import routes from '../routes'
import StorageService from '../services/StorageService'

class Home extends Component {

    render() {
        return (
            <div className='home container level' style={{ flexDirection: 'column', padding: '50px 0' }}>
                <h2 className="title">Welcome to UserApp</h2>
                <h2 className="subtitle">Fullstack test assignment for PovioLabs, by Matija Jeras</h2>
                {
                    !StorageService.getToken() &&
                    <div className="level">
                        <Link className="button" to={routes.login}>Login</Link>
                        <span style={{ margin: '0 10px' }}>or</span>
                        <Link className="button is-link" to={routes.sign_up}>Sign up</Link>
                        <span style={{ margin: '0 10px' }}>|</span>
                        <Link className="button" to={routes.users}>User List</Link>
                    </div>
                }
            </div>
        )
    }

}
Home = inject('authStore')(observer(Home))
export default Home