/**
 * Class to operate with storage
 */

class StorageService {
    constructor() {
        this.storage = window.localStorage;
        this.token_key = 'token';
        this.current_user_key = 'current_user'
    }

    getCurrentUser() {
        return this.storage.getItem(this.current_user_key) || false;
    }

    setCurrentUser(user) {
        if (user) {
            this.storage.setItem(this.current_user_key, JSON.stringify(user));
        }
    }

    removeCurrentUser() {
        this.storage.removeItem(this.current_user_key);
    }

    getToken() {
        return this.storage.getItem(this.token_key) || false;
    }

    setToken(token) {
        if (token) {
            this.storage.setItem(this.token_key, token);
        }
    }

    removeToken() {
        this.storage.removeItem(this.token_key);
    }

    getSearchData() {
        let data = this.storage.getItem(this.form_key) || false;
        if (data) {
            return JSON.parse(data)
        }
    }

    setSearchData(data) {
        if (data) {
            this.storage.setItem(this.form_key, JSON.stringify(data));
        }
    }

    removeSearchData() {
        this.storage.removeItem(this.form_key);
    }

}

export default new StorageService()