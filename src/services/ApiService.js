import api from '../api';
import StorageService from './StorageService'

class ApiService {

    constructor() {
        this.api_url = 'http://localhost:5000/api';
    }

    async apiCall(url, method = 'GET', token = false, params = null) {
        let payload = {
            method,
            mode: 'cors',
            headers: this.buildHeaders(token),
        }
        if (params) {
            payload.body = JSON.stringify(params);
        }
        const res = await fetch(`${this.api_url}${url}`, payload);
        const status = res.status;
        const body = await res.json();
        return { status, body };
    }
    
    buildHeaders(token = false) {
        let headers = new Headers();
        headers.append('Content-type', 'application/json');
        if (token) {
            headers.append('x-access-token', token);
        }

        return headers;
    }

    handleCommonError(response, auth = false) {
        if(response.status === 401 && auth) {
            StorageService.removeToken()
            window.location(api.login)
        }
        if (response.status !== 200 && response.status !== 201) {
            throw new Error(response.status)
        }
        return;
    }

    async current_user(token = false) { //load current user profile
        if (!token) {
            throw new Error('Error loading profile. Missing token!')
        }
        const res = await this.apiCall('/me', 'GET', token);
        this.handleCommonError(res);
        return res.body;
    }
}

export default new ApiService()