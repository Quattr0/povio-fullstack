// server/models/User.js
const mongoose = require('mongoose')
var bcrypt = require('bcryptjs')

let UserSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
    },
    likes: {
        type:  Number,
        required: true,
        default: 0,
    },
    likedBy: {
        type:  Array,
        required: true,
        default: [],
    }
})

//hashing a password before saving it to the database
UserSchema.pre('save', function (next) {
    var user = this
    if (!user.password || user.password.charAt(0) === '$') {
        next()
    }
    bcrypt.hash(user.password, 10, function (err, hash) {
        if (err) {
            return next(err)
        }
        user.password = hash
        next()
    })
})

module.exports = mongoose.model('User', UserSchema)