var User = require('../models/User');

/**
 * Configure JWT
 */
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var bcrypt = require('bcryptjs');
var config = require('../auth/config'); // get config file

module.exports = {
    login: (req, res) => {

        User.findOne({ username: req.body.username }, (err, user) => {
            if (err) return res.status(500).json({ message: 'Server error.' });
            if (!user) return res.status(404).json({ message: 'User not found.' });
            
            // check if the password is valid
            bcrypt.compare(req.body.password, user.password, (err, resp) => {
                if (!resp) {
                    return res.status(401).json({ auth: false, token: null, message: 'Wrong username or password.' });
                }

                var token = jwt.sign({ id: user._id }, config.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });
          
                return res.status(201).json({ auth: true, token: token });
            });
        });
    },
    me: (req, res) => {
        User.findById(req.userId, { password: 0 }, (err, user) => {
            if (err) return res.status(500).json({ message: 'Server error.' });
            if (!user) return res.status(404).json({ message: 'User not found.' });
            return res.status(201).json({ user });
        });
    },
    changePassword: (req, res) => {
        User.findById(req.userId, (err, user) => {
            if (err) return res.status(500).json({ message: 'Server error.' });
            if (!user) return res.status(404).json({ message: 'User not found.' });
            bcrypt.compare(req.body.currPassword, user.password, (err, resp) => {
                if (!resp) {
                    return res.status(401).json({ auth: false, token: null, message: 'Current password is incorrect.' });
                }
                user.password = req.body.newPassword;
                user.save();
                return res.status(201).json({ message: 'Success!' });
            });
        });
    },
    likeUser: (req, res) => {

        if (req.userId === req.params.id) {
            return res.status(403).json({ message: 'You cannot like yourself!' });
        }

        User.findById(req.params.id, { password: 0 }, (err, user) => {
            if (err) return res.status(500).json({ message: 'Server error.' });
            if (!user) return res.status(404).json({ message: 'User not found.' });

            if (user.likedBy.includes(req.userId)) {
                return res.status(403).json({ message: 'You already like this user.' });
            }
            user.likedBy.push(req.userId);
            user.likes++;
            user.save();

            return res.status(201).json({ message: 'Success!' });
        });
    },
    unlikeUser: (req, res) => {
        if (req.userId === req.params.id) {
            return res.status(403).json({ message: 'You cannot unlike yourself!' });
        }

        User.findById(req.params.id, { password: 0 }, (err, user) => {
            if (err) return res.status(500).json({ message: 'Server error.' });
            if (!user) return res.status(404).json({ message: 'User not found.' });

            if (!user.likedBy.includes(req.userId)) {
                return res.status(403).json({ message: 'You do not like this user.' });
            }

            const index = user.likedBy.indexOf(req.userId);
            user.likedBy.splice(index, 1);
            user.likes--;
            user.save();

            return res.status(201).json({ message: 'Success!' });
        });
    },
}