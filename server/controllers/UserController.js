const User = require('./../models/User')

module.exports = {
    getMostLiked: (req, res) => {
        User.find().sort({ 'likes': -1 }).lean().exec(function (err, users) {
            return res.status(201).json({ users });
        });
    },
    getUser: (req, res) => {
        User.findById(req.params.id).lean().exec(function (err, user) {
            return res.status(201).json({ user });
        });
    },
    create: (req, res) => {
        let { username, password } = req.body
        if (password.charAt(0) === '$') {
            return res.status(403).json({ message: 'Password cannot begin with "$"' });
        }
        User.findOne({ username: username }, (err, user) => {
            if (err) {
                return res.send(err)
            }

            if (!user) {
                User.create({ username: username, password: password }, (err, small) => {
                    if (err) {
                        return res.status(500).json({ message: 'Error creating user', error: err });
                    };
                    return res.status(201).json({ message: 'Success!' });
                });
            } else {
                return res.status(400).json({ message: 'Username "' + username + '" is already taken' });
            }
        });
    },
    logout: (req, res) => {
        return res.status(201).json({ auth: false, token: null });
    }
}