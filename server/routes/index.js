const UserController = require('../controllers/UserController')
const AuthController = require('../controllers/AuthController')
var VerifyToken = require('../auth/VerifyToken');

module.exports = (router) => {

    // Add new user
    router
        .route('/signup')
        .post(UserController.create)

    // Login user
    router
        .route('/login')
        .post(AuthController.login)

    // Logout user
    router
        .route('/logout')
        .post(UserController.logout)

    // Info about current user
    router
        .route('/me')
        .get(VerifyToken, AuthController.me)

    // Change password
    router
        .route('/me/update-password')
        .post(VerifyToken, AuthController.changePassword)

    // Get most liked users
    router
        .route('/most-liked')
        .get(UserController.getMostLiked)

    // User info
    router
        .route('/user/:id')
        .get(UserController.getUser)

    // Like user
    router
        .route('/user/:id/like')
        .post(VerifyToken, AuthController.likeUser)

    // Unlike user
    router
        .route('/user/:id/unlike')
        .post(VerifyToken, AuthController.unlikeUser)
}