let expect = require('chai').expect,
    supertest = require('supertest'),
    api = supertest('http://localhost:5000/api');

describe('User tests', function () {

    let token;
    let userId;

    it('should create user', function (done) {
        api.post('/signup')
            .send({
                username: 'Test',
                password: 'test123',
            })
            .expect(201)
            .end(function (err, res) {
                expect(res.body).to.have.property('message');
                expect(res.body.message).to.equal('Success!');
                done();
            });
    });

    it('should fail creating another user with the same username', function (done) {
        api.post('/signup')
            .send({
                username: 'Test',
                password: 'pass123',
            })
            .expect(403)
            .end(function (err, res) {
                expect(res.body).to.have.property('message');
                expect(res.body.message).to.equal('Username "Test" is already taken');
                done();
            });
    });

    it('should log in previously created user', function (done) {
        api.post('/login')
            .send({
                username: 'Test',
                password: 'test123',
            })
            .expect(201)
            .end(function (err, res) {
                expect(res.body).to.have.property('auth');
                expect(res.body.auth).to.equal(true);
                expect(res.body).to.have.property('token');
                expect(res.body.token).to.not.equal(null);
                token = res.body.token;
                done();
            });
    });

    it('should fail logging in user with incorrect password', function (done) {
        api.post('/login')
            .send({
                username: 'Test',
                password: 'incorrect',
            })
            .expect(401)
            .end(function (err, res) {
                expect(res.body).to.have.property('auth');
                expect(res.body.auth).to.equal(false);
                expect(res.body).to.have.property('token');
                expect(res.body.token).to.equal(null);
                expect(res.body).to.have.property('message');
                expect(res.body.message).to.equal('Wrong username or password.');
                done();
            });
    });

    it('should get current user info if token in sent in header', function (done) {
        api.get('/me')
            .set('x-access-token', token)
            .expect(201)
            .end(function (err, res) {
                expect(res.body).to.have.property('user');
                expect(res.body.user).to.have.property('_id');
                expect(res.body.user._id).to.not.equal(null);
                userId = res.body.user._id;
                done();
            });
    });

    it('should update "Test" user\'s password if token in sent', function (done) {
        api.post('/me/update-password')
            .set('x-access-token', token)
            .send({
                currPassword: 'test123',
                newPassword: 'test345',
            })
            .expect(201)
            .end(function (err, res) {
                expect(res.body).to.have.property('message');
                expect(res.body.message).to.equal('Success!');
                done();
            });
    });

    it('should get user\'s info by ID', function (done) {
        api.get('/user/' + userId)
            .expect(201)
            .end(function (err, res) {
                expect(res.body).to.have.property('user');
                expect(res.body.user).to.have.property('likes');
                expect(res.body.user.likes).to.equal(0);
                expect(res.body.user).to.have.property('username');
                expect(res.body.user.username).to.equal('Test')
                done();
            });
    });

    it('should fail if user likes himself', function (done) {
        api.post('/user/' + userId + '/like')
            .set('x-access-token', token)
            .expect(403)
            .end(function (err, res) {
                expect(res.body).to.have.property('message');
                expect(res.body.message).to.equal('You cannot like yourself!')
                done();
            });
    });

    it('should fail if user unlikes himself', function (done) {
        api.post('/user/' + userId + '/unlike')
            .set('x-access-token', token)
            .expect(403)
            .end(function (err, res) {
                expect(res.body).to.have.property('message');
                expect(res.body.message).to.equal('You cannot unlike yourself!')
                done();
            });
    });

    it('should get most-liked user list', function (done) {
        api.get('/most-liked')
            .expect(201)
            .end(function (err, res) {
                expect(res.body).to.have.property('users');
                expect(res.body.users).to.be.a('array');
                expect(res.body.users).to.have.lengthOf(1);
                done();
            });
    });
});